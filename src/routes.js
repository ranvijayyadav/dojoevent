import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Home , Admin} from "./containers";
import {NavBar} from './components'
export default () => {
    return (
        <Router>
            <div class="container-fluid">
                <NavBar />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/admin" component={Admin} />
                </Switch>
            </div>
        </Router>
    );
};
