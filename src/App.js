import React, { Component } from 'react'
import {NavBar} from './components'
///import {App} from './containers';
import AppRoutes from './routes';
import './App.css'
class App extends Component {
  render() {
    return (
      <div>   
        <AppRoutes />
      </div>
    )
  }
}
export default App