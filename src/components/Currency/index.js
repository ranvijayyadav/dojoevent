import React, { Component } from 'react'

import dime from './../../assets/images/dime.jpg';
import dollar from './../../assets/images/dollar.jpg';
import nickel from './../../assets/images/nickel.jpg';
import quarter from './../../assets/images/quarter.jpg';

export default class Currency extends Component {
    constructor(){
        super()
        this.state={}
    }
    clickNickelHandler=()=>{
        //this.
        this.props.paymentDone(0.01)
    }
    clicDimekHandler=()=>{
        //this.
        this.props.paymentDone(0.05)
    }
    clicQuarterkHandler=()=>{
        //this.
        this.props.paymentDone(0.25)
    }
    clickDollarHandler=()=>{
        this.props.paymentDone(1)
    }
    render(){
        return(
<div class="row">
        <div class="col-sm-3" onClick={this.clickNickelHandler}>
            <div class="card">
                <div class="card-header">
                    <img src={nickel} width="" height="" alt="Nickel" />
                </div>
                <div class="card-body">
                    Nickel<br/>
                    <span>&#36;0.01</span>
                </div>
            </div>
        </div>
        
        <div class="col-sm-3" onClick={this.clicDimekHandler}>
            <div class="card">
                <div class="card-header">
                    <img src={dime} width="" height="" alt="Nickel" />
                </div>
                <div class="card-body">
                    Dime<br/>
                    <span>&#36;0.05</span>
                </div>
            </div>
        </div>
        
        <div class="col-sm-3" onClick={this.clicQuarterkHandler}>
            <div class="card">
                <div class="card-header">
                    <img src={quarter} width="" height="" alt="Nickel" />
                </div>
                <div class="card-body">
                    Quarter<br/>
                    <span>&#36;0.25</span>
                </div>
            </div>
        </div>
        
        <div class="col-sm-3" onClick={this.clickDollarHandler}>
            <div class="card">
                <div class="card-header">
                    <img src={dollar} width="" height="" alt="Nickel" />
                </div>
                <div class="card-body">
                    Dollar<br/>
                    <span>&#36;1.00</span>
                </div>
            </div>
        </div>
          
    </div>
    )
    }
}